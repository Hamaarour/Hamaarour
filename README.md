<h1 align="center">Hi there 👋 , I'm Hicham Amaarour</h1>

 <div align="center">
<!--   <a href="https://github.com/Hamaarour/Hamaarour">
  <img src="https://badge.mediaplus.ma/greenbinary/hamaarou" alt="hamaarou's 42 stats" />
  </a> -->
</div>


 

<h3 align="center">░▒▓█ WHOAMI █▓▒░</h3>

<p align="center">
As a Computer Science Student at 1337 Benguerir, I am deeply passionate about programming and software development, with a particular focus on web development and front-end technologies. I always strive to assist and support others in their journey - feel free to "ᴀꜱᴋ ᴍᴇ ᴀʙᴏᴜᴛ ᴀɴʏᴛʜɪɴɢ, ɪ'ᴍ ʜᴀᴘᴘʏ ᴛᴏ ʜᴇʟᴘ 🙂" <br>
My unwavering commitment to progress and consistency drives my enthusiasm for the field. I am dedicated to being the best version of myself, continuously pushing my boundaries and seeking new challenges. It is in these moments of growth and evolution that I find my comfort and purpose.
I have set clear and personal goals, measured not against others, but against my own past self. My top priority is self-improvement, aiming to be a better version of who I was yesterday.<br>
Programming has not only enriched my technical skills but also transformed my thinking, enhancing my problem-solving abilities. The process of learning and growing in this field is an experience I thoroughly enjoy, echoing Leonardo da Vinci's wisdom that 'Learning never exhausts the mind.'
<br>

<p align="center">
  <font color="purple"> >_ "Just keep swimming."</font>
I find my inspiration in these encouraging words from my best friend. They serve as a constant reminder to persevere through life's challenges, maintain resilience, and steadfastly continue moving forward.
</p>
</p>


 
<h3 align="center">░▒▓█ LET'S CONNECT █▓▒░</h3>
  
  
  
 
<p align="center">
	<a href="mailto:amaarour.hicham97@gmail.com">
	<img alt="Feel free to contact me" src="https://img.shields.io/badge/-Email-%238B3ED2?style=flat&logo=Gmail&logoColor=white&link=mailto:amaarour.hicham97@gmail.com" />
	</a>
	<a href="https://www.linkedin.com/in/hicham-amaarour-5a1b84220/">
	<img alt="Linkedin" src="https://img.shields.io/badge/-Linkedin-%238B3ED2?style=flat&logo=Linkedin&logoColor=white&link=https:https://www.linkedin.com/in/hicham-amaarour-5a1b84220/" />
	</a>
	<a href="https://https://discord.com/users/690657837067796522">
	<img alt="Discordd Profile" src="https://img.shields.io/badge/-Discord-%238B3ED2?style=flat&logo=discord&logoColor=white" />
	</a>
</p>


<div align="center">
	
<h3>░▒▓█ 1337 PROJECTS █▓▒░</h3>


<a href="https://github.com/Hamaarour/libft-1337"><img src="https://cdn.discordapp.com/attachments/780570837505540126/897951891395313725/libfte.png"></a>
<a href="https://github.com/Hamaarour/ft_printf_1337"><img src="https://github.com/ablaamim/ft_printf/blob/main/SRC/ft_printfm.png"></a>
<a href="https://github.com/Hamaarour/get_next_line_1337"><img src="https://github.com/ablaamim/Get_Next_Line/blob/main/SRC/get_next_linem.png"></a>
<a href="https://github.com/Hamaarour/Born2Beroot_1337"><img src="https://github.com/ablaamim/Born2BeRoot/blob/main/SRC/born2berootm.png"></a>
<a href="https://github.com/Hamaarour/So_Long_42"><img src="https://cdn.discordapp.com/attachments/780570837505540126/974802342400655360/so_long.png">
<a href="https://github.com/Hamaarour/Minitalk_42"><img src="https://github.com/Hamaarour/Minitalk_42/blob/main/minitalkm.png">
</a>
<a href="https://github.com/Hamaarour/push_swap"><img src="https://github.com/Hamaarour/push_swap/blob/main/push_swapm.png">
<a href="https://github.com/Hamaarour/minishell"><img src="https://github.com/Hamaarour/minishell/blob/Parsing/minishell.png">	
<a href="https://github.com/Hamaarour/Philosopher"><img src="https://github.com/Hamaarour/Philosopher/blob/main/assets/philosophers.png">	
<a href="https://github.com/Hamaarour/CPP_Modules"><img src="https://github.com/Hamaarour/CPP_Modules/blob/main/assets/cppe.png">

<a href="https://github.com/Hamaarour/Net_Practice"><img src="https://github.com/Hamaarour/Net_Practice/blob/main/assets/netpracticee.png">
<a href="https://github.com/Hamaarour/Cub3D_1337"><img 
src="https://github.com/Hamaarour/Cub3D_1337/blob/parsing/assets/cub3de.png">


 
</div>






---



<div align="center"><h3>░▒▓█ LANGUAGES & TOOLS █▓▒░</h3></div>



<table width="100" align="center">
	<!-- ROW 1 -->
<tr>
    <td align='center' width="190">
        <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> 
	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> 
	</a> 
    </td>
    <td align='center' width="190">
        <a href="https://www.w3schools.com/cs/" target="_blank" rel="noreferrer"> 
	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg" alt="csharp" width="40" height="40"/> 
	</a>
    </td>
    <td align='center' width="190">
        <a href="https://en.cppreference.com/w/" target="_blank" rel="noreferrer">
	<img src="https://cdn.worldvectorlogo.com/logos/c.svg" alt="c++" width="40" height="40"/>
	</a> 
    </td>
     <td align='center' width="190">
       <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> 
	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a>
	<br>
    </td>
    <td align='center'  width="190">
        <a href="https://dotnet.microsoft.com/" target="_blank" rel="noreferrer"> 
	  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/dot-net/dot-net-original-wordmark.svg" alt="dotnet" width="40" height="40"/>
	</a> 
    </td>
</tr>
	<!-- ROW 2-->
<tr>
    <td align='center' width="190">
       <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> 
	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/>
	</a>
    </td>
    <td align='center' width="190">
       <img src="https://www.vectorlogo.zone/logos/tailwindcss/tailwindcss-icon.svg" alt="tailwind" width="40" height="40">
    </td>
     <td align='center' width="190">
         <img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" alt="React" width="40" height="40">
    </td>
    <td align='center'>
       <a>
          <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> 
       </a> 
     </td>
	<td align='center'>
          <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer">
	    <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> 
	  </a>
         </td>
</tr>
	<!-- ROW 3-->
<tr>
  <td align='center'>
        <a href="https://www.microsoft.com/en-us/sql-server" target="_blank" rel="noreferrer"> 
	<img src="https://www.svgrepo.com/show/303229/microsoft-sql-server-logo.svg" alt="mssql" width="40" height="40"/> 
	</a> 
  </td>
 <td align='center'>
  <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> 
	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> 
	</a>
 </td>
<tr/>
	<!-- ROW 4-->
<tr>
    <td align='center' width="190">
        <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> 
  	<img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" 		target="_blank" rel="noreferrer">
    </td>
    <td align='center' width="190">
        <img src="https://github.githubassets.com/images/modules/logos_page/Octocat.png" alt="command-line" width="40" height="40">
    </td>
    <td align='center' width="190" >
        <img src="https://img.icons8.com/color/2x/command-line.png" alt="gitub" width="40" height="40">
    </td>
</tr>
<!-- ROW 5-->
<tr>
    <td align='center' width="190">
	<a href="https://www.linux.org/" target="_blank" rel="noreferrer">
  	<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> 
  	</a>
    </td>
	<td align='center' width="190">
	<a href="" target="_blank" rel="noreferrer">
  	<img src="https://www.freeiconspng.com/uploads/brushed-metal-apple-mac-icon-29.png" alt="MacOs" width="40" height="40"> 	
  	</a>
    </td>
     </td>
	<td align='center' width="190">
	<a href="" target="_blank" rel="noreferrer">
  	<img src="https://www.freeiconspng.com/uploads/microsoft-windows-logo-png-5.png" alt="Windows" width="40" height="40"> 	
  	</a>
	</td>
</table>
<!-- -->

---------------
| [![Hamaarour's GitHub stats](https://github-readme-stats.vercel.app/api?username=Hamaarour&count_private=true&show_icons=true&hide=issues&hide_border=true&theme=jolly)](https://github.com/Hamaarour?tab=repositories) | [![Hamaarour's most used languages](https://github-readme-stats.vercel.app/api/top-langs/?username=Hamaarour&layout=compact&hide_border=true&theme=jolly)](https://github.com/Hamaarour?tab=repositories) | 
|:-:|:-:|
	

</div> 



![Codewars rank](https://www.codewars.com/users/Hicham%20Amaarour/badges/large)



<p align="center">
<img alt="Hamaarour's visitors" src="https://komarev.com/ghpvc/?username=Hamaarour&color=8c36db&style=flat&label=visitors" />
<img alt="Hamaarour's followers" src="https://img.shields.io/github/followers/Hamaarour?color=blueviolet" />
<img alt="Hamaarour's stars" src="https://img.shields.io/github/stars/Hamaarour?color=blueviolet" />
</p>














